import ReactDOM from 'react-dom';
import App from './view/App';

const root = document.getElementById('root');

if (!root) {
  throw new Error('root not found');
}

ReactDOM.render(<App />, root);
