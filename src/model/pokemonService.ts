import { Pokemon } from '@/model/pokemon';

export interface IPokemonService {
  getPokemons(offset: number, limit: number): Promise<Pokemon[]>;
  getTotalPokemonsCount(): number;
  catchPokemon(pokemon: Pokemon): Promise<void>;
  getTotalCaughtPokemonsCount(): number;
  isPokemonCaught(id: number): boolean;
  getCatchDate(id: number): string | undefined;
  getCaughtPokemons(page: number, limit?: number): Promise<Pokemon[]>;
  getPokemonByIdOrName(idOrName: string): Promise<Pokemon>;
}
