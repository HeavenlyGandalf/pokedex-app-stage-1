import { Pokemon } from '@/model/pokemon';

export interface IPokemonStore {
  catchPokemon(pokemon: Pokemon): void;
  getCaughtPokemons(): Pokemon[];
  isPokemonCaught(id: number): boolean;
  getTotalCaughtPokemonsCount(): number;
  getPokemonById(id: number): Pokemon | undefined;
  getCatchDate(id: number): string | undefined;
}
