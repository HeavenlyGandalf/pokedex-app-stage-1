export interface Ability {
  name: string;
  slot: number;
}

export interface Pokemon {
  id: number;
  name: string;
  imageUrl: string;
  isCaught: boolean;
  abilities?: Ability[];
  catchDate?: string;
}

export interface PokemonSummary {
  name: string;
  url: string;
}
