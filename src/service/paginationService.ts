import { IPaginationService } from '@/model/paginationService';

export class PaginationService implements IPaginationService {
  private currentPage: number = 1;
  private totalItems: number = 0;
  private itemsPerPage: number = 20;

  public getCurrentPage(): number {
    return this.currentPage;
  }

  public getTotalItems(): number {
    return this.totalItems;
  }

  public getItemsPerPage(): number {
    return this.itemsPerPage;
  }

  public setTotalItems(totalItems: number): void {
    this.totalItems = totalItems;
  }

  public nextPage(): void {
    if (this.currentPage < this.getTotalPages()) {
      this.currentPage++;
    }
  }

  public prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  public getTotalPages(): number {
    return Math.ceil(this.totalItems / this.itemsPerPage);
  }
}
