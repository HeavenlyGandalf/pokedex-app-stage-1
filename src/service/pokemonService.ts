import { Pokemon, PokemonSummary } from '@/model/pokemon';
import { PokemonApi } from '../transport/pokemonApi';
import { PokemonStore } from '@/store/pokemonStore';
import { IPokemonService } from '@/model/pokemonService';

export class PokemonService implements IPokemonService {
  private totalPokemonsCount: number = 0;
  private pokemonStore: PokemonStore;
  private pokemonApi: PokemonApi;

  constructor() {
    this.pokemonStore = PokemonStore.getInstance();
    this.pokemonApi = new PokemonApi();
  }

  public async getPokemons(offset: number, limit: number) {
    const data = await this.pokemonApi.fetchPokemons(offset, limit);
    this.totalPokemonsCount = data.count;

    const pokemonPromises = data.results.map(
      async (pokemonSummary: PokemonSummary) => {
        const details = await this.pokemonApi.fetchPokemonDetails(
          pokemonSummary.url
        );
        return {
          id: details.id,
          name: details.name,
          imageUrl: details.sprites.front_default,
          isCaught: this.pokemonStore.isPokemonCaught(details.id),
        };
      }
    );

    const pokemons = await Promise.all(pokemonPromises);
    return pokemons;
  }

  public getTotalPokemonsCount(): number {
    return this.totalPokemonsCount;
  }

  public async catchPokemon(pokemon: Pokemon): Promise<void> {
    this.pokemonStore.catchPokemon(pokemon);
  }

  public getTotalCaughtPokemonsCount(): number {
    return this.pokemonStore.getTotalCaughtPokemonsCount();
  }

  public isPokemonCaught(id: number): boolean {
    return this.pokemonStore.isPokemonCaught(id);
  }

  public getCatchDate(id: number): string | undefined {
    return this.pokemonStore.getCatchDate(id);
  }

  public async getCaughtPokemons(
    page: number,
    limit: number = 20
  ): Promise<Pokemon[]> {
    const allCaughtPokemons = this.pokemonStore.getCaughtPokemons();
    const offset = (page - 1) * limit;
    const paginatedCaughtPokemons = allCaughtPokemons.slice(
      offset,
      offset + limit
    );
    return paginatedCaughtPokemons.map((pokemon) => ({
      ...pokemon,
      isCaught: true,
    }));
  }

  public async getPokemonByIdOrName(idOrName: string): Promise<Pokemon> {
    const details = await this.pokemonApi.fetchPokemonDetailsByIdOrName(
      idOrName
    );
    return {
      id: details.id,
      name: details.name,
      imageUrl: details.sprites.front_default,
      isCaught: this.pokemonStore.isPokemonCaught(details.id),
      abilities: details.abilities.map(
        (abilityData: { ability: { name: string }; slot: number }) => ({
          name: abilityData.ability.name,
          slot: abilityData.slot,
        })
      ),
    };
  }
}
