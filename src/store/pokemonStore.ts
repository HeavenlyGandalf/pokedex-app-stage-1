import { Pokemon } from '@/model/pokemon';
import { IPokemonStore } from '@/model/pokemonStore';

export class PokemonStore implements IPokemonStore {
  private static instance: PokemonStore;
  private caughtPokemons: Set<Pokemon> = new Set();

  public static getInstance(): PokemonStore {
    if (!PokemonStore.instance) {
      PokemonStore.instance = new PokemonStore();
    }
    return PokemonStore.instance;
  }

  public catchPokemon(pokemon: Pokemon): void {
    const caughtDate = new Date()
      .toLocaleString('ru-RU', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false,
      })
      .replace(',', '');

    const caughtPokemon = {
      ...pokemon,
      catchDate: caughtDate,
      isCaught: true,
    };
    this.caughtPokemons.add(caughtPokemon);
  }

  public getCaughtPokemons(): Pokemon[] {
    return Array.from(this.caughtPokemons);
  }

  public isPokemonCaught(id: number): boolean {
    return Array.from(this.caughtPokemons).some((pokemon) => pokemon.id === id);
  }

  public getTotalCaughtPokemonsCount(): number {
    return this.caughtPokemons.size;
  }

  public getPokemonById(id: number): Pokemon | undefined {
    return Array.from(this.caughtPokemons).find((pokemon) => pokemon.id === id);
  }

  public getCatchDate(id: number): string | undefined {
    const caughtPokemon = Array.from(this.caughtPokemons).find(
      (pokemon) => pokemon.id === id
    );
    return caughtPokemon ? caughtPokemon.catchDate : undefined;
  }
}
