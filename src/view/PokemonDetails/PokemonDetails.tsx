import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { PokemonService } from '@/service/pokemonService';
import { Pokemon } from '@/model/pokemon';
import styles from './pokemonDetails.module.scss';
import { Loader } from '../Loader/Loader';

const PokemonDetails: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const navigate = useNavigate();

  const handleBackClick = () => {
    navigate(-1);
  };
  const [pokemon, setPokemon] = useState<Pokemon | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [catchDate, setCatchDate] = useState<string | undefined>(undefined);

  useEffect(() => {
    const fetchPokemon = async () => {
      const pokemonService = new PokemonService();
      const fetchedPokemon = await pokemonService.getPokemonByIdOrName(id);
      setCatchDate(pokemonService.getCatchDate(Number(id)));
      setPokemon(fetchedPokemon);
      setLoading(false);
    };

    fetchPokemon();
  }, [id]);

  if (loading) {
    return <Loader />;
  }

  if (!pokemon) {
    return <p>Pokemon not found</p>;
  }

  return (
    <div className={styles.pokemonDetails}>
      <button className={styles.backButton} onClick={handleBackClick}></button>
      <div className={styles.pokemonContainer}>
        <img
          className={styles.pokemonImage}
          src={pokemon.imageUrl}
          alt={pokemon.name}
        />
        <div className={styles.pokemonName}>{pokemon.name}</div>
        <div className={styles.pokemonId}>{`ID: ${pokemon.id}`}</div>
        <div className={styles.pokemonAbilities}>
          <div>Abilities:</div>
          {pokemon.abilities.map((ability) => (
            <div key={ability.name} className={styles.ability}>
              {ability.name}
            </div>
          ))}
        </div>
        <div className={styles.pokemonStatus}>
          <div className={styles.statusTitle}>Status:</div>
          <div className={styles.statusText}>
            {pokemon.isCaught ? 'Пойман' : 'Не пойман'}
          </div>
        </div>
        {pokemon.isCaught && (
          <div className={styles.catchDate}>Дата поимки: {catchDate}</div>
        )}
      </div>
    </div>
  );
};

export default PokemonDetails;
