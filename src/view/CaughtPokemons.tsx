import React, { useEffect, useRef, useState, useCallback } from 'react';
import { PokemonService } from '@/service/pokemonService';
import { Pokemon } from '@/model/pokemon';
import styles from './styles/styles.module.scss';
import PokemonCard from './PokemonCard/PokemonCard';
import { NoPokemonPage } from './NoPokemon/NoPokemonPage';
import { Loader } from './Loader/Loader';

const CaughtPokemons: React.FC = () => {
  const [caughtPokemons, setCaughtPokemons] = useState<Pokemon[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const observer = useRef<IntersectionObserver | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const pokemonService = new PokemonService();

  useEffect(() => {
    const fetchCaughtPokemons = async () => {
      const newPokemons = await pokemonService.getCaughtPokemons(currentPage);
      setCaughtPokemons((prevPokemons) => [...prevPokemons, ...newPokemons]);
      setLoading(false);
    };

    fetchCaughtPokemons();
  }, [currentPage]);

  const loadMore = useCallback(() => {
    setLoading(true);
    setCurrentPage((prevPage) => prevPage + 1);
  }, []);

  const lastPokemonElementRef = useCallback(
    (node: HTMLDivElement) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          loadMore();
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, loadMore]
  );

  return (
    <div className={styles.pokemons}>
      {caughtPokemons.length === 0 ? (
        <NoPokemonPage />
      ) : (
        caughtPokemons.map((pokemon, index) => {
          if (caughtPokemons.length - 1 === index) {
            return (
              <div ref={lastPokemonElementRef} key={pokemon.id}>
                <PokemonCard pokemon={pokemon} onCatch={() => {}} />
              </div>
            );
          } else {
            return (
              <PokemonCard
                key={pokemon.id}
                pokemon={pokemon}
                onCatch={() => {}}
              />
            );
          }
        })
      )}
      {loading && <Loader />}
    </div>
  );
};

export default CaughtPokemons;
