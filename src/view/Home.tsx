import React, { useEffect, useRef, useState } from 'react';
import { PokemonService } from '@/service/pokemonService';
import { PaginationService } from '@/service/paginationService';
import styles from './styles/styles.module.scss';
import PokemonCard from './PokemonCard/PokemonCard';
import { Pokemon } from '@/model/pokemon';
import { Loader } from './Loader/Loader';

interface HomeProps {
  onCatch: () => void;
}

const Home: React.FC<HomeProps> = ({ onCatch }) => {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const paginationService = useRef(new PaginationService()).current;
  const pokemonService = useRef(new PokemonService()).current;

  useEffect(() => {
    loadPokemons();
  }, [currentPage]);

  const loadPokemons = async () => {
    setIsLoading(true);
    try {
      const offset = (currentPage - 1) * paginationService.getItemsPerPage();
      const limit = paginationService.getItemsPerPage();

      const pokemons = await pokemonService.getPokemons(offset, limit);
      const totalPokemons = await pokemonService.getTotalPokemonsCount();
      paginationService.setTotalItems(totalPokemons);

      pokemons.forEach((pokemon: Pokemon) => {
        pokemon.isCaught = pokemonService.isPokemonCaught(pokemon.id);
      });

      setPokemons(pokemons);
    } catch (error) {
      console.error('Failed to load pokemons', error);
    } finally {
      setIsLoading(false);
    }
  };

  const catchPokemon = async (pokemon: Pokemon) => {
    await pokemonService.catchPokemon(pokemon);
    onCatch();
    loadPokemons();
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      paginationService.prevPage();
      setCurrentPage(paginationService.getCurrentPage());
    }
  };

  const handleNextPage = () => {
    if (currentPage < paginationService.getTotalPages()) {
      paginationService.nextPage();
      setCurrentPage(paginationService.getCurrentPage());
    }
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <div className={styles.pokemons}>
            {pokemons.map((pokemon) => (
              <PokemonCard
                key={pokemon.id}
                pokemon={pokemon}
                onCatch={() => catchPokemon(pokemon)}
              />
            ))}
          </div>

          <div id='pagination' className={styles.pagination}>
            <button
              id='prev'
              onClick={handlePrevPage}
              disabled={currentPage === 1}
            >
              &lt;
            </button>
            <span id='page-info'>{currentPage}</span>
            <button
              id='next'
              onClick={handleNextPage}
              disabled={currentPage === paginationService.getTotalPages()}
            >
              &gt;
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default Home;
