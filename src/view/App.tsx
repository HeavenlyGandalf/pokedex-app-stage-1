import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './Home';
import CaughtPokemons from './CaughtPokemons';
import Header from './Header/Header';
import PokemonDetails from './PokemonDetails/PokemonDetails';
import NotFound from './NotFound/NotFound';
import { PokemonService } from '@/service/pokemonService';

const App: React.FC = () => {
  const [caughtCount, setCaughtCount] = useState<number>(0);

  const pokemonService = new PokemonService();
  const handlePokemonCaught = async () => {
    const count = await pokemonService.getTotalCaughtPokemonsCount();
    setCaughtCount(count);
  };

  return (
    <Router>
      <Header caughtCount={caughtCount} />
      <Routes>
        <Route path='/' element={<Home onCatch={handlePokemonCaught} />} />
        {/* {exact насколько я поняла не используется в этой версии, но уже учитывается} */}
        <Route path='/caught' element={<CaughtPokemons />} />
        <Route path='/pokemon/:id' element={<PokemonDetails />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
    </Router>
  );
};

export default App;
