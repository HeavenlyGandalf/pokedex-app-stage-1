import styles from './Loader.module.scss';

export function Loader() {
  return (
    <div className={styles.loader}>
      <div className={styles.ball}>
        <img
          className={styles.image}
          src={'https://pngimg.com/uploads/pokeball/pokeball_PNG12.png'}
          alt='Loading'
        />
      </div>
      <div className={styles.loadingText}>
        Catching them all... Please wait!
      </div>
    </div>
  );
}
