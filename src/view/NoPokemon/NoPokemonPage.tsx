import styles from './NoPokemonPage.module.scss';

export function NoPokemonPage() {
  return (
    <div className={styles.noPokemonContainer}>
      <div>
        <h1>It seems you don't have any Pokemon caught yet!</h1>
        <p>
          Don't get upset! The world is full of amazing Pokemon that are waiting
          for you! 🌟
        </p>
        <p>
          <strong>Tips for successful hunting:</strong>
        </p>
        <ul>
          <li>
            Search at different times of the day: Some Pokemon are active during
            the day, others at night.
          </li>
          <li>
            Explore different places: Pokemon can be in the most unexpected
            locations.
          </li>
          <li>Use baits: They will attract Pokemon to you.</li>
          <li>
            Participate in events: This is a chance to catch rare Pokemon.
          </li>
        </ul>
        <p>Good luck, Coach!</p>
        <p>
          Every Pokemon you catch brings you closer to the rank of Pokemon
          Master.
        </p>
      </div>
      <img
        src={
          'https://avatanplus.com/files/resources/original/57b037a8b2e0e15688596b1d.png'
        }
        alt='Bulbasaur'
        className={styles.bulbasaurImg}
      />
    </div>
  );
}

export default NoPokemonPage;
