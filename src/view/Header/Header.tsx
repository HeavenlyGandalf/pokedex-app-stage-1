import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styles from './header.module.scss';

interface HeaderProps {
  caughtCount: number;
}

const Header: React.FC<HeaderProps> = ({ caughtCount }) => {
  const location = useLocation();
  const [activeTab, setActiveTab] = useState<string>('/');

  useEffect(() => {
    setActiveTab(location.pathname);
  }, [location.pathname]);

  return (
    <header>
      <div className={styles.tabs}>
        <Link
          to='/'
          className={`${styles.tab} ${activeTab === '/' ? styles.active : ''}`}
        >
          Главная
        </Link>
        <Link
          to='/caught'
          className={`${styles.tab} ${
            activeTab === '/caught' ? styles.active : ''
          }`}
        >
          Пойманные покемоны
        </Link>
      </div>
      <span className={styles.caughtCount}>{caughtCount}</span>
    </header>
  );
};

export default Header;
